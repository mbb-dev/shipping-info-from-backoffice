""" Configuration for shipping data from backoffice """

ESSERVER = 'localhost:9200'
ESALIAS = 'shipping-info-dev'
SOLR_SERVER = '192.168.10.31:8983'
SOLR_CORE = '/solr/shipping'
SOLR_STEPSIZE = 10000


def config():
    """ Returns a dictionary of type SimpleNamespace containing the
    configuration setting.
    """
    return {
        'es_index': ESALIAS,
        'es_host': ESSERVER,
        'solr_server' :SOLR_SERVER,
        'solr_core': SOLR_CORE,
        'solr_stepsize': SOLR_STEPSIZE,
        }
