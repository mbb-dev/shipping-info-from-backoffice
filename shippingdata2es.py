""" Transfer shipping data from backend to Elasticsearch """

import re
import settings
from library import elasticsearch
from general import configuration
from library.solr import BackofficeShippingData


class ShippingDataCollector:
    """ Collects shipping data from backoffice and stores them in an
    Elasticsearch index
    """

    def __init__(self, conf, ):
        """ Initializes the collector : store configuration settings """
        self._conf = conf

    def __call__(self):
        """ collect all shipping data from backoffice and store them in an
        elasticsearch index
        """
        datafeed = BackofficeShippingData(self._conf)
        url = 'http://%s/%s' % (
            self._conf['es_host'], self._conf['es_index'])
        if not elasticsearch.index_exists(url):
            elasticsearch.index_create(url)
        docs = []
        count = 0
        for line in datafeed():
            docs.append(self.shippingdata(line))
            count += 1
            if count % 1000 == 0:
                print('%s shipping info from backoffice collected' % count)
        print('%s shipping info from backoffice collected' % count)
        result = elasticsearch.insert_many(
            '%s/_bulk' % url, self._conf['es_index'], docs)
        print('Documents written to Elasticsearch index %s' %
              self._conf['es_index'])
        return result

    def shippingdata(self, line):
        """ Returns a json document ready to be imported into an Elasticsearch
        Index
        """
        doc = {
            "_id": line['id'].replace('.', '|'),
            "RetailerID": line['RetailerID'],
            "ShippingCost": line['ShippingAndHandling'],
            "DeliveryInfo": line['DeliveryPeriod'],
            "CountryCode": line['CountryCode'],
            "Source": "backoffice",
            }
        return doc

def run(conf, args):
    """ Main part, start working """
    collector = ShippingDataCollector(
        configuration.merge_settings(conf, args))
    collector()


if __name__ == '__main__':
    CONF = settings.config()
    ARGS = configuration.get_arguments()
    run(CONF, ARGS)
