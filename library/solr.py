""" Provide an iterator over documents from Solr """

import requests


class BackofficeShippingData:
    """ Provide an iterator over the shipping data from our backoffice """

    def __init__(self, conf):
        """ Note configuration settings and initialize common variables """
        self._conf = conf
        self._stepsize = 10000
        self._query = self._querystring()

    def __call__(self):
        next_doc = '!' # first character in ASCII code
        last_doc = None
        while not next_doc == last_doc:
            last_doc = next_doc
            url = ''.join([
                'http://',
                self._conf.get('solr_server', ''),
                self._conf.get('solr_core', ''),
                '/',
                self._querystring().format(next=next_doc),
                '&wt=json',
                ])
            response = requests.get(url).json()
            documents = response.get('response', {}).get('docs', [])
            if documents:
                for shipping_doc in documents:
                    yield shipping_doc
                next_doc = documents[-1].get('id', next_doc)

    def _querystring(self):
        """ Build querystring to question Solr """
        template = (
            'select?&q=id:[{next} TO *]'
            '&sort=id asc'
            '&start=0'
            '&rows={rows}'
            '&fl=id,CountryCode,DeliveryPeriod,RetailerID,ShippingAndHandling'
            '&indent=false'
            )
        # fill in rows parameter but keep next
        querystr = template.format(rows=self._stepsize, next='{next}')
        if self._conf['solr_stepsize']:
            querystr = template.format(
                rows=self._conf['solr_stepsize'],
                next='{next}')
        return querystr
