""" Elasticsearch stuff for ShippingData2ES """

import json
import requests


def index_exists(url):
    response = requests.head(url)
    return response.ok


def index_create(url):
    payload = {
        'mappings': {
            'shipping': {
                'properties': {
                    'RetailerID': {'type': 'integer'},
                    'RetailerProductID': {'type': 'keyword'},
                    'ShippingCost': {'type': 'text'},
                    'DeliveryInfo': {'type': 'text'},
                    'CountryCode': {'type': 'keyword'},
                    'Source': {'type': 'keyword'},
                    }
                }
            }
        }
    response = requests.put(url, json=payload)
    return response.ok


def insert_many(url, index, docs):
    payload = ''
    for doc in docs:
        action = {
            'index': {
                '_index': index,
                '_type': 'shipping',
                '_id': doc['_id']}
            }
        doc.pop('_id')
        payload += '%s\n' % json.dumps(action)
        payload += '%s\n' % json.dumps(doc)
    headers = {'Content-Type': 'application/json'}
    response = requests.put(url, data=payload, headers=headers)
    if response.json()['errors']:
        print("!!! There have been errors.")
        print(response.content)
    return response.ok
