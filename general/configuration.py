""" Configuration for ShippingData2ES """
import argparse

def get_arguments():
    """ Read commandline arguments """
    parser = argparse.ArgumentParser(
        description="Transfer shipping data from backoffice to ES")
    return parser.parse_args()

def merge_settings(conf, args):
    """ Merge configuration settings from different sources

    conf    Dictionary with configuration settings
    args    SimpleNamespace object containing command line arguments
    """
    result = {}
    for key in conf:
        result[key] = conf[key]
    for key in vars(args):
        result[key] = getattr(args, key)
    return result
